module Room where

data Room = Room { id :: Int
                 , number :: String
                 , capacity :: Int } deriving (Show, Eq)

newRoom :: Int -> String -> Int -> Room
newRoom i n c = Room { Room.id = i
                     , number = n
                     , capacity = c }
