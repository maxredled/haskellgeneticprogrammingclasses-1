module Main where

import Data.Maybe
import System.Environment
import System.Exit
import System.Random
import Graphics.EasyPlot
import Init
import Class
import Course
import Professor
import Room
import School
import TimeSlot
import Individual
import Population
import Timetable

helpMessage :: IO ()
helpMessage = do
  putStrLn "To run this program type:"	
  putStrLn "geneticClasses, max number of iterations, size of the population, mutation rate, crossover rate, number of elite elements that are preserved butween generations, tournament size"
  putStrLn "geneticClasses Int Int Float Float Int Int"
  
readn :: String -> Int
readn s = read s :: Int

main :: IO ()
main = do 
  args <- getArgs
  if head args == "--help" || null args
    then helpMessage
    else case length args of
      6 -> do
        let iter = read $ head args -- max number of generations
        let size = read (args !! 1) -- population size
        let mutationRate = read (args !! 2) :: Float
        let crossoverRate = read (args !! 3) :: Float
        let elite = read (args !! 4) :: Int -- elitism allows the fittest individual,
                                            -- or individuals, to be added unaltered 
                                            -- to the next generations population. 
                                            -- Size of elit population
        let tournamentSize = read (args !! 5) :: Int
  
        school <- fillSchool

        g <- newStdGen
        p <- createPopulation size school
        let population = calcFitness school p
  
        fitness <- loop 0 iter elite tournamentSize mutationRate crossoverRate 
                        school population

        print $ head fitness
        print $ last fitness
        printGraphic fitness

        print "End of program, aditional output in graphic Fitness.png" 

      _ -> do
        putStrLn "Invalid input, missing arguments"
        putStrLn "Type --help"

loop :: Int -> Int -> Int -> Int -> Float -> Float -> School -> Population
     -> IO [ (Int, Int, Int, Int) ]
loop _ 0 _ _ _ _ _ _ = do
  putStrLn "Did not find solution"
  return []
loop n iter e tSize m c s p = do -- curent iteration, max iterations, elite, tournament size, mutation rate, crossover rate, school, previous population 
  p' <- newGeneration e tSize m c s p
  let f = fromMaybe (error "Fitness not available, module Main, function loop") 
                    (fitness (head $ ordPopulation p'))

  if f == 0 
    then do
        printSolution (head $ ordPopulation p') s
	return [ (n, f, calcFitnessPopulation p', length p') ]
    else do
      rest <- loop (n+1) (iter-1) e tSize m c s p'
      return $ (n, f, calcFitnessPopulation p', length p') : rest

printGraphic :: [ (Int, Int, Int, Int) ] -> IO Bool
printGraphic fitness =
  plot (PNG "Fitness.png") 
    [ Data2D [ Title "Population Fitness", Style Lines, Color Red ] []  
      $ map (\ (i, _, fp, _) -> (fromIntegral i, fromIntegral fp) ) fitness
    , Data2D [ Title "Best Individual Fitness", Style Lines, Color Blue ] []
      $ map (\ (i, f, _, _) -> (fromIntegral i, fromIntegral f) ) fitness
    , Data2D [ Title "Total Population", Style Lines, Color Green ] []
      $ map (\ (i, _, _, size) -> (fromIntegral i, fromIntegral size) ) fitness ]

printSolution :: Individual -> School -> IO ()
printSolution i s = do
  let t = createClasses i s -- creates timetable from selected chromosome
  let output = map (\ cl ->  "Group: " ++ show (group cl)
               ++ ", Course: " ++ Course.name (findCourse s (course cl))
               ++ ", Professor: " ++ Professor.name (findProfessor s (professor cl))
               ++ ", Time slot: " ++ description (findTimeSlot s (timeSlot cl))
               ++ ", Room: " ++ number (findRoom s (room cl))
	       ) t
  putStrLn "--------------------"
  putStrLn "Found time table is:"
  mapM_ print output
  putStrLn "--------------------"



