module Population_Test where

import Room
import Professor
import Course
import Group
import TimeSlot
import Class
import Individual
import Population
import School
import Timetable
import Test.HUnit

test_calcFitness1 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  TestCase $ assertEqual "Test clashes for population of 1"
			 [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                                , (3,7), (4,9), (5,6) ]
			              , fitness = Just 2 } ]  
			 p' 

test_calcFitness2 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } 
	  , Chromosome { genes = [ (0,7), (1,9), (2,6)
	                         , (3,7), (4,9), (5,6) ]
		       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  TestCase $ assertEqual "Test clashes for population of 2"
			 [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                                , (3,7), (4,9), (5,6) ]
			              , fitness = Just 2 } 
			 , Chromosome { genes = [ (0,7), (1,9), (2,5)
			                        , (3,7), (4,9), (5,6) ]
				      , fitness = Just 4 } ]
			 p' 

test_calcFitnessPopulation1 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  let pf = calcFitnessPopulation p' in
  TestCase $ assertEqual "Test population fitness for population of 1"
			 2 pf 

test_calcFitnessPopulation2 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } 
	  , Chromosome { genes = [ (0,7), (1,9), (2,6)
	                         , (3,7), (4,9), (5,6) ]
		       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  let pf = calcFitnessPopulation p' in
  TestCase $ assertEqual "Test population fitness for population of 2"
			 6 pf 

test_ordPopulation1 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = ordPopulation $ calcFitness s p in
  TestCase $ assertEqual "Test order for population of 1"
			 [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                                , (3,7), (4,9), (5,6) ]
			              , fitness = Just 2 } ]  
			 p' 

test_ordPopulation2 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } 
	  , Chromosome { genes = [ (0,7), (1,9), (2,6)
	                         , (3,7), (4,9), (5,6) ]
		       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  let p' = ordPopulation $ calcFitness s p in
  TestCase $ assertEqual "Test order for population of 2"
			 [ Chromosome { genes = [ (0,7), (1,9), (2,5)
                                                , (3,7), (4,9), (5,6) ]
			              , fitness = Just 2 } 
			 , Chromosome { genes = [ (0,7), (1,9), (2,5)
			                        , (3,7), (4,9), (5,6) ]
				      , fitness = Just 4 } ]
			 p' 

test_ordPopulation3 = 
  let p = [ Chromosome { genes = [ (0,7), (1,9), (2,6)
                                 , (3,7), (4,9), (5,6) ]
                       , fitness = Nothing } 
	  , Chromosome { genes = [ (0,7), (1,9), (2,5)
	                         , (3,7), (4,9), (5,6) ]
		       , fitness = Nothing } ] in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let p' = calcFitness s p in
  let p' = ordPopulation $ calcFitness s p in
  TestCase $ assertEqual "Test order for population of 2, switching form previous test"
			 [ Chromosome { genes = [ (0,7), (1,9), (2,5)
			                        , (3,7), (4,9), (5,6) ]
				      , fitness = Just 2 }
			 , Chromosome { genes = [ (0,7), (1,9), (2,5)
                                                , (3,7), (4,9), (5,6) ]
			              , fitness = Just 4 } ]
			 p' 

main = runTestTT $ TestList [ test_calcFitness1             -- 0
			    , test_calcFitness2             -- 1 
			    , test_calcFitnessPopulation1   -- 2
			    , test_calcFitnessPopulation2   -- 3 
			    , test_ordPopulation1           -- 4
			    , test_ordPopulation2           -- 5
			    , test_ordPopulation3 ]
