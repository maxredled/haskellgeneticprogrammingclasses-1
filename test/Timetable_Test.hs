module Timetable_Test where

import Room
import Professor
import Course
import Group
import TimeSlot
import Class
import Individual
import Population
import School
import Timetable
import Test.HUnit

test_newTimetable1 = 
  let rs = [ newRoom 5 "A2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 2 [ 3, 4 ] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  TestCase $ assertEqual "Test if constructor works"
                         (head rs) $ findRoom s 5

test_newTimetable2 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5)
                               , (3,7), (4,9), (5,5) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 15 ] in 
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 2 [ 3, 4 ] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let cs = createClasses c s in
  TestCase $ assertEqual "Test if creating a timetable from a chromosome works"
                         [ newClass 0 1 3 7 9 5 
		         , newClass 1 1 4 7 9 5 ] cs

test_newTimetable3 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5)
                               , (3,6), (4,99), (5,6)
                               , (6,7), (7,99), (8,6)
                               , (9,6), (10,9), (11,5) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "A2" 25  ] in
  let ps = [ newProfessor 7 "Jim", newProfessor 6 "Joe" ] in
  let cs = [ newCourse 3 "Sci101" "Sci" [ 6 ], newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8], newGroup 11 29 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00", newTimeSlot 99 "Mon 10:00 - 12:00" ] in
  let s = newSchool rs ps cs gs ts in
  let cs = createClasses c s in
  TestCase $ assertEqual "Test if creating a more complicated timetable from a chromosome works"
                         [ newClass 0 1 3 7 9 5
	                 , newClass 1 1 8 6 99 6
			 , newClass 2 11 3 7 99 6
			 , newClass 3 11 8 6 9 5 ] cs

test_calcClashes1 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 9 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let t = createClasses c s in
  TestCase $ assertEqual "Test no clashes"
			 0 (calcClashes t s)

test_calcClashes2 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let t = createClasses c s in
  TestCase $ assertEqual "Test number students greater than room size"
			 1 (calcClashes t s)

test_calcClashes3 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5)
                               , (3,7), (4,9), (5,6) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 25 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let t = createClasses c s in
  TestCase $ assertEqual "Test same professor at same time slot"
			 1 (calcClashes t s)

test_calcClashes4 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5)
                               , (3,8), (4,9), (5,5) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 25 ] in
  let ps = [ newProfessor 7 "Jim", newProfessor 8 "Joe" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let t = createClasses c s in
  TestCase $ assertEqual "Test same room at same time slot"
			 1 (calcClashes t s)

test_calcClashes5 = 
  let c = Chromosome { genes = [ (0,7), (1,9), (2,5)
                               , (3,7), (4,9), (5,6) ]
                     , fitness = Nothing } in
  let rs = [ newRoom 5 "A2" 25, newRoom 6 "B2" 15 ] in
  let ps = [ newProfessor 7 "Jim" ] in
  let cs = [ newCourse 8 "Mat101" "Math" [ 7 ] ] in
  let gs = [ newGroup 1 19 [ 3, 8] ] in
  let ts = [ newTimeSlot 9 "Mon 08:00 - 10:00" ] in
  let s = newSchool rs ps cs gs ts in
  let t = createClasses c s in
  TestCase $ assertEqual "Test professor at same time slot and group grater than room size"
			 2 (calcClashes t s)

main = runTestTT $ TestList [ test_newTimetable1  -- 0
                            , test_newTimetable2  -- 1
			    , test_newTimetable3  -- 2 
			    , test_calcClashes1   -- 3 
			    , test_calcClashes2   -- 4
			    , test_calcClashes3   -- 5
			    , test_calcClashes4   -- 6 
			    , test_calcClashes5 ] -- 7
