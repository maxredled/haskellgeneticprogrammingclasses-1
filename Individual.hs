module Individual where

import Professor
import TimeSlot
import Room
import School
import Lib

-- A chromosome is a sequense of prof, time slot, room 
data Individual = Individual { genes :: [ (Int, Int) ] -- [ (pos, chromosome) ]
                             , fitness :: Maybe Int } -- number of clashes
	            deriving Show

instance Eq Individual where
  Individual { fitness = a } == Individual { fitness = b } = Just a == Just b

instance Ord Individual where
  compare Individual { fitness = a } Individual { fitness = b } = compare (Just a) (Just b)

newIndividual :: [ (Int, Int) ] -> Maybe Int -> Individual 
newIndividual gs f = Individual { genes = gs
                                , fitness = f }

createChromosome :: School -> IO [ Int ]
createChromosome s = do
  let n = nbrClasses s
  let createChromosome' n = do
        p <- rand $ map Professor.id (School.professors s)
        t <- rand $ map TimeSlot.id (School.timeSlots s)
        r <- rand $ map Room.id (School.rooms s)
        if n == 0 
          then return []
          else do
            rest <- createChromosome' (n-1)
            return ([ p, t, r] ++ rest)
  createChromosome' n
