module TimeSlot where

data TimeSlot = TimeSlot { id :: Int
                         , description :: String } deriving (Show, Eq)

newTimeSlot :: Int -> String -> TimeSlot
newTimeSlot i d = TimeSlot { TimeSlot.id = i
                           , description = d }
